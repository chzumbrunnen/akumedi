<?php
/**
 * The header for AKUMEDI theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AkuMedi
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="wrapper">

  <div id="space_up"></div><!--end  space_up -->
  
   <div id="header">
 
     <?php 
        $post_id = get_queried_object_id();

        if ( has_post_thumbnail($post_id) ) { // check if the post has a Post Thumbnail assigned to it.
            the_post_thumbnail($post_id);
        } 
    ?>
        
   </div><!--end header -->
   
    
    <?php
        if (class_exists('MultiPostThumbnails')) :

       $secondpostfeaturedimage =  MultiPostThumbnails::get_post_thumbnail_url(
            get_post_type(),
            'secondary-image'
        );
        endif;
    ?> 
    
   <div id="left" style="background: url( <?=$secondpostfeaturedimage ; ?> ); background-clip: content-box; height: 538px;">
       &nbsp;
    <?php if ( is_front_page() && is_active_sidebar( 'announcements-1' ) ): ?>
        <ul id="sidebar">
            <?php dynamic_sidebar( 'announcements-1' ); ?>
        </ul>
    <?php endif; ?>
       
    </div><!--end left -->
    
<div id="right">
  
           
    <nav id="site-navigation" class="main-navigation" role="navigation">
        <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'akumedi' ); ?></button>
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
    </nav><!-- #site-navigation -->
        
    
    <div id="text_field"> <!-- div im footer schliessen -->
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AkuMedi
 */

?>
    
    
   </div><!--end text_field -->
    
  </div><!--end right -->
  
<div id="footer">
    <div id="kontaktdaten">
        <?php get_sidebar(); ?>
    </div>
</div><!--end footer -->
  


</div><!-- #wrapper -->

<?php wp_footer(); ?>
</body>
</html>